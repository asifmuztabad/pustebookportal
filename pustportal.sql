-- phpMyAdmin SQL Dump
-- version 4.4.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Oct 23, 2015 at 11:57 PM
-- Server version: 5.6.26
-- PHP Version: 5.6.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pustportal`
--

-- --------------------------------------------------------

--
-- Table structure for table `bookstore`
--

CREATE TABLE IF NOT EXISTS `bookstore` (
  `id` int(10) unsigned NOT NULL,
  `filetitle` varchar(1000) COLLATE utf8_unicode_ci NOT NULL,
  `bookfilepath` varchar(1000) COLLATE utf8_unicode_ci NOT NULL,
  `fileauthor` varchar(1000) COLLATE utf8_unicode_ci NOT NULL,
  `mimetype` varchar(1000) COLLATE utf8_unicode_ci NOT NULL,
  `downloadaddress` varchar(1000) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `filetitle1` varchar(1000) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `bookstore`
--

INSERT INTO `bookstore` (`id`, `filetitle`, `bookfilepath`, `fileauthor`, `mimetype`, `downloadaddress`, `created_at`, `updated_at`, `filetitle1`) VALUES
(20, '1304.3629.pdf', 'D:\\Xampp\\htdocs\\PustEbookPortal/public/uploadebook\\1304.3629.pdf', 'asdfsdf', 'application/pdf', 'asdfsdf', '2015-10-10 20:33:11', '2015-10-10 20:33:11', ''),
(21, '521asifmit02.pdf', 'D:\\Xampp\\htdocs\\PustEbookPortal/public/uploadebook\\5213ijmit02.pdf', 'asdasd', 'application/pdf', 'sdasd', '2015-10-12 08:51:04', '2015-10-12 08:51:04', ''),
(22, 'asif', 'D:\\Xampp\\htdocs\\PustEbookPortal/public/uploadebook\\asif', 'asd', 'application/pdf', 'aaaa', '2015-10-21 23:22:39', '2015-10-21 23:22:39', '5213ijmit02.pdf');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE IF NOT EXISTS `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2015_10_09_000157_maketable', 1),
('2015_10_11_024649_create_users_table', 2),
('2015_10_11_033814_create_password_resets_table', 3),
('2015_10_11_045449_create_users_table', 3);

-- --------------------------------------------------------

--
-- Table structure for table `reset`
--

CREATE TABLE IF NOT EXISTS `reset` (
  `id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL,
  `fullname` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `username` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `fullname`, `username`, `password`, `email`, `phone`, `created_at`, `updated_at`, `remember_token`) VALUES
(1, 'Asif Muztaba', 'asif', '$2y$10$LM/Fl8ALMR6jbXNrpIhCWuSDxZo6nsJJwCOad/qzuLkslYs9AeT7G', 'amuztaba18@gmail.com', '123', '2015-10-10 22:59:27', '2015-10-19 15:53:16', 'cfooPt5wDcx9l4b2FE4swSTz47VKPi7O7bJ2ZkVxLhMfVhia25qjETDhfwlS'),
(2, 'shithil', 'shithil', '$2y$10$AUmqnKC9j5RyczqmWP1JtOH2KkFFdFxbK4L4b7p.2F.m9xjFMheZm', 'amuztaba18@gmail.com', '123', '2015-10-19 16:03:52', '2015-10-19 16:03:52', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bookstore`
--
ALTER TABLE `bookstore`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `reset`
--
ALTER TABLE `reset`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bookstore`
--
ALTER TABLE `bookstore`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT for table `reset`
--
ALTER TABLE `reset`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
