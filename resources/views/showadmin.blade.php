@extends('adminmaster')

@section('content')
<div class="col s12 m12">
						<div class="searchForm animated slideInDown " style="display:none">
							<form action="" method="get" class="">
								<div class="input-field">		    
								<input id="searchIn1" id="search" type="text" class="validate" name="bookname">	
								<label for="searchIn1" class="center-align">Search E-Books Here..</label>	    	
								<input id="searchIn2" class="btn" type="submit" value="Search">	
								</div>
							</form>
							<style>
td, th {
    padding: 15px 5px;
    display: table-cell;
    text-align: center;
    vertical-align: middle;
    border-radius: 2px;
}
							</style>
						</div>
						@include('adminMenu')
						@if(!empty($users))
						 <div class="container-fluid aboutH">
						        <h5 class="copper">User List</h5>
						        <h6></h6>
						        <hr>
						        <div class="row">
						          <div class="col s12 m12">
								      <table class="bordered">
								        <thead>
								          <tr>
								          	  <th data-field="id">ID</th>
								              <th data-field="id">Full Name</th>
								              <th data-field="name">Email</th>
								              <th data-field="price">Username </th>
								              <th data-field="price">Phone No.</th>
								              <th data-field="price">Created At</th>
								              <th data-field="price">Edite</th>
								              <th data-field="price">Delete</th>
								              
								          </tr>
								        </thead>
@foreach($users as $property)
								        <tbody>
								          <tr>
								            <td>{{$property->id}}</td>
								            <td>{{$property->fullname}}</td>
								            <td>{{$property->email}}</td>
								            <td>{{$property->username}}</td>
								            <td>{{$property->phone}}</td>
								            <td>{{$property->created_at}}</td>
								            <td><a href="{{ URL::to('editeuser',$property->id) }}" class="waves-effect waves-light btn"><i class="fa fa-edit left"></i>Edite</a></td>
								          	<td><a href="{{ URL::to('deleteUser',$property->id) }}" class="waves-effect waves-light btn"><i class="fa fa-delete left"></i>Delete</a></td>
								          </tr>

@endforeach
								        </tbody>
								      </table>
						          </div>
						          </div>

						   </div>

						   @endif


						   						<div class="main_content animated slideInDown">
								<ul class="ca-menu">
									<li>
										<a href="#">
											<span class="ca-icon">A</span>
											<div class="ca-content">
												<h2 class="ca-main">New Books</h2>
												<h3 class="ca-sub">Recent Uploaded Books</h3>
											</div>
										</a>
									</li>
									<li>
										<a href="#">
											<span class="ca-icon">B</span>
											<div class="ca-content">
												<h2 class="ca-main">Most Downloaded</h2>
												<h3 class="ca-sub">Most Downloaded Books</h3>
											</div>
										</a>
									</li>
									<li>
										<a href="#">
											<span class="ca-icon">C</span>
											<div class="ca-content">
												<h2 class="ca-main">Popular Books</h2>
												<h3 class="ca-sub">Popular E-Books</h3>
											</div>
										</a>
									</li>
								</ul>
						</div>

					</div>
					@stop
