<div id="login-page" class="row">
    <div class="col s12 z-depth-4 card-panel adminpanel">
    	<a href="{{ URL::to('adminPanel') }}" class="btn adminbtn">AdminHome</a>
    	<a href="{{ URL::to('showadmin') }}" class="btn adminbtn">Show All Admin</a>
		<a href="{{ URL::to('registerUser') }}" class="btn adminbtn">Register New Admin</a>
		<a href="{{ URL::to('upload') }}" class="btn adminbtn">Upload New Books</a>
		<a href="{{ URL::to('showbooks/A') }}" class="btn adminbtn">Show All</a>
		<a href="{{ URL::to('deletebooks/A') }}" class="btn adminbtn">Delete</a>
    </div>
  </div>