<div class="main_content animated slideInDown">
								<ul class="ca-menu">
									<li>
										<a href="#">
											<span class="ca-icon">A</span>
											<div class="ca-content">
												<h2 class="ca-main">New Books</h2>
												<h3 class="ca-sub">Recent Uploaded Books</h3>
											</div>
										</a>
									</li>
									<li>
										<a href="#">
											<span class="ca-icon">B</span>
											<div class="ca-content">
												<h2 class="ca-main">Most Downloaded</h2>
												<h3 class="ca-sub">Most Downloaded Books</h3>
											</div>
										</a>
									</li>
									<li>
										<a href="#">
											<span class="ca-icon">C</span>
											<div class="ca-content">
												<h2 class="ca-main">Popular Books</h2>
												<h3 class="ca-sub">Popular E-Books</h3>
											</div>
										</a>
									</li>
								</ul>
						</div>

					</div>