@extends('adminmaster')

@section('content')

<div class="col s12 m12">
						<div class="searchForm animated slideInDown " style="display:block">
<a href="{{ URL::to('showbooks','A') }}">A</a> |
<a href="{{ URL::to('showbooks','B') }}">B</a> |
<a href="{{ URL::to('showbooks','C') }}">C</a> |
<a href="{{ URL::to('showbooks','D') }}">D</a> |
<a href="{{ URL::to('showbooks','E') }}">E</a> |
<a href="{{ URL::to('showbooks','F') }}">F</a> |
<a href="{{ URL::to('showbooks','G') }}">G</a> |
<a href="{{ URL::to('showbooks','H') }}">H</a> |
<a href="{{ URL::to('showbooks','I') }}">I</a> |
<a href="{{ URL::to('showbooks','J') }}">J</a> |
<a href="{{ URL::to('showbooks','K') }}">K</a> |
<a href="{{ URL::to('showbooks','L') }}">L</a> |
<a href="{{ URL::to('showbooks','M') }}">M</a> |
<a href="{{ URL::to('showbooks','N') }}">N</a> |
<a href="{{ URL::to('showbooks','O') }}">O</a> |
<a href="{{ URL::to('showbooks','P') }}">P</a> |
<a href="{{ URL::to('showbooks','Q') }}">Q</a> |
<a href="{{ URL::to('showbooks','R') }}">R</a> |
<a href="{{ URL::to('showbooks','S') }}">S</a> |
<a href="{{ URL::to('showbooks','T') }}">T</a> |
<a href="{{ URL::to('showbooks','U') }}">U</a> |
<a href="{{ URL::to('showbooks','V') }}">V</a> |
<a href="{{ URL::to('showbooks','W') }}">W</a> |
<a href="{{ URL::to('showbooks','X') }}">X</a> |
<a href="{{ URL::to('showbooks','Y') }}">Y</a> |
<a href="{{ URL::to('showbooks','Z') }}">Z</a> |


							<style>
td, th {
    padding: 15px 5px;
    display: table-cell;
    text-align: center;
    vertical-align: middle;
    border-radius: 2px;
}
							</style>
						</div>
						@include('adminMenu')
						@if($users!=='0')
						 <div class="container-fluid aboutH">
						        <h5 class="copper">BookList By Alphabetic Order</h5>
						        <h6></h6>
						        <hr>
						        <div class="row">
						          <div class="col s12 m12">
								      <table class="bordered">
								        <thead>
								          <tr>
								              <th data-field="id">Ebook Name</th>
								              <th data-field="name">Author</th>

								              <th data-field="price">Edite</th>
								          </tr>
								        </thead>
@foreach($users as $property)

								        <tbody>

								          <tr>
								            <td>{{$property->filetitle}}</td>
								            <td>{{$property->fileauthor}}</td>
								            <td><a href="{{ URL::to('editbook',$property->id) }}" class="waves-effect waves-light btn"><i class="fa fa-edit left"></i>Edite</a></td>
								          </tr>
								           

@endforeach
								        </tbody>
								      </table>
						          </div>
						          </div>
{!! $users->render() !!}
  <span class="paginationCustomText"> Showing {{($users->currentpage()-1)*$users->perpage()+1}} to {{$users->currentpage()*$users->perpage()}}
    of  {{$users->total()}} E-Books
</span>
						   </div>
@else
						 <div class="container-fluid aboutH">
						        <h5 class="copper">About Us</h5>
						        <h6>Let Us introduce Ourself</h6>
						        <hr>
						        <div class="row">
						          <div class="col s12 m12">
<h1>Sorry no Books Found!!!!</h1>
						          </div>
						          </div>

						   </div>

						   @endif



						   						<div class="main_content animated slideInDown">
								<ul class="ca-menu">
									<li>
										<a href="#">
											<span class="ca-icon">A</span>
											<div class="ca-content">
												<h2 class="ca-main">New Books</h2>
												<h3 class="ca-sub">Recent Uploaded Books</h3>
											</div>
										</a>
									</li>
									<li>
										<a href="#">
											<span class="ca-icon">B</span>
											<div class="ca-content">
												<h2 class="ca-main">Most Downloaded</h2>
												<h3 class="ca-sub">Most Downloaded Books</h3>
											</div>
										</a>
									</li>
									<li>
										<a href="#">
											<span class="ca-icon">C</span>
											<div class="ca-content">
												<h2 class="ca-main">Popular Books</h2>
												<h3 class="ca-sub">Popular E-Books</h3>
											</div>
										</a>
									</li>
								</ul>
						</div>

					</div>
					@stop