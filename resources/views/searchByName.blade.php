@extends('master')

@section('content')
<div class="col s8 m8">
						<div class="searchForm animated slideInDown " style="display:block">
							<form action="" method="get" class="">
								<div class="input-field">		    
								<input id="searchIn1" id="search" type="text" class="validate" name="bookname">	
								<label for="searchIn1" class="center-align">Search E-Books Here..</label>	    	
								<input id="searchIn2" class="btn" type="submit" value="Search">	
								</div>
							</form>
							<style>
td, th {
    padding: 15px 5px;
    display: table-cell;
    text-align: center;
    vertical-align: middle;
    border-radius: 2px;
}
							</style>
						</div>
						@if(!empty($users))
						 <div class="container-fluid aboutH">
						        <h5 class="copper">About Us</h5>
						        <h6>Let Us introduce Ourself</h6>
						        <hr>
						        <div class="row">
						          <div class="col s12 m12">
								      <table class="bordered">
								        <thead>
								          <tr>
								              <th data-field="id">Ebook Name</th>
								              <th data-field="name">Author</th>
								              <th data-field="price">Download Link</th>
								          </tr>
								        </thead>
@foreach($users as $property)
								        <tbody>
								          <tr>
								            <td>{{$property->filetitle}}</td>
								            <td>{{$property->fileauthor}}</td>
								            <td><a href="{{ URL::to('getentry',$property->filetitle) }}" class="waves-effect waves-light btn"><i class="fa fa-download left"></i>Download</a></td>
								          </tr>

@endforeach
								        </tbody>
								      </table>
						          </div>
						          </div>
{!! $users->render() !!}
  <span class="paginationCustomText"> Showing {{($users->currentpage()-1)*$users->perpage()+1}} to {{$users->currentpage()*$users->perpage()}}
    of  {{$users->total()}} E-Books
</span>
						   </div>

						   @endif


						   						<div class="main_content animated slideInDown">
								<ul class="ca-menu">
									<li>
										<a href="#">
											<span class="ca-icon">A</span>
											<div class="ca-content">
												<h2 class="ca-main">New Books</h2>
												<h3 class="ca-sub">Recent Uploaded Books</h3>
											</div>
										</a>
									</li>
									<li>
										<a href="#">
											<span class="ca-icon">B</span>
											<div class="ca-content">
												<h2 class="ca-main">Most Downloaded</h2>
												<h3 class="ca-sub">Most Downloaded Books</h3>
											</div>
										</a>
									</li>
									<li>
										<a href="#">
											<span class="ca-icon">C</span>
											<div class="ca-content">
												<h2 class="ca-main">Popular Books</h2>
												<h3 class="ca-sub">Popular E-Books</h3>
											</div>
										</a>
									</li>
								</ul>
						</div>

					</div>
					@stop