<!doctype html>
<html lang="eng">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0"/>
		<title>PUST-E-Book Download Portal</title>
		 <!--Import Google Icon Font-->
		<link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
		<!--Import materialize.css-->
		<link type="text/css" rel="stylesheet" href="{{ URL::asset('css/materialize.min.css')}}"  media="screen,projection"/>
		 <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
		 <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">

		<link href="{{ URL::asset('css/style.css')}}" type="text/css" rel="stylesheet" media="screen,projection"/>
		<link rel="stylesheet" href="{{ URL::asset('css/animate.min.css')}}">
		<link href="{{ URL::asset('css/home_spin.css')}}" type="text/css" rel="stylesheet" media="screen,projection"/>

	</head>
	<body>
		<div id="wrapper">
			<div class="container-fluid">