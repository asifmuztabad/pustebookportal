<div class="col s8 m8">
						<div class="searchForm animated slideInDown " style="display:none">
							<form action="" method="" class="">
								<div class="input-field">		    
								<input id="searchIn1" placeholder="Search E-Books Here.." id="search" type="text" class="validate">	
									    	
								<input id="searchIn2" class="btn" type="submit" value="Search">	
								</div>
							</form>
						</div>
						 <div class="container-fluid aboutH">
						        <h5 class="copper">About Us</h5>
						        <h6>Let Us introduce Ourself</h6>
						        <hr>
						        <div class="row">
						          <div class="col s12 m12">
						            <h6 class="copper">Story</h6>
						            <div class="row">
						            	<div class="col s4 m4 l4">
						            		<div class="imagebox">
						            			<img src="{{ URL::asset('img/office.jpg')}}" alt="">
						            		</div>
						            	</div>
						            	<div class="col s8 m8 l8">
						            		<div class="imgcontent">
						            			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
						            		</div>
						            	</div>
						            </div>
						            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
						          </div>
						          </div>
						   </div>
						   						<div class="main_content animated slideInDown">
								<ul class="ca-menu">
									<li>
										<a href="#">
											<span class="ca-icon">A</span>
											<div class="ca-content">
												<h2 class="ca-main">New Books</h2>
												<h3 class="ca-sub">Recent Uploaded Books</h3>
											</div>
										</a>
									</li>
									<li>
										<a href="#">
											<span class="ca-icon">B</span>
											<div class="ca-content">
												<h2 class="ca-main">Most Downloaded</h2>
												<h3 class="ca-sub">Most Downloaded Books</h3>
											</div>
										</a>
									</li>
									<li>
										<a href="#">
											<span class="ca-icon">C</span>
											<div class="ca-content">
												<h2 class="ca-main">Popular Books</h2>
												<h3 class="ca-sub">Popular E-Books</h3>
											</div>
										</a>
									</li>
								</ul>
						</div>

					</div>