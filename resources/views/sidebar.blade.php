					<div class="col s4 m4 l4">
						<div class="side animated slideInRight">
							<p class="sideTitle"> <i class="fa fa-database"></i>Total Number Of E-Books</p>
								<div class="sideBoxContent">
									<p><i id="book"class="fa fa-book"></i> 10</p>
								</div>
						</div>
						<div class="side animated slideInRight">
							<p class="sideTitle"><i class="fa fa-connectdevelop"></i>Stay Up to Date With What's Happening</p>
								<div class="sideBoxContent">
									<ul class="faico clear">
							          <li><a class="faicon-twitter" href="#"><i class="fa fa-twitter"></i></a></li>
							          <li><a class="faicon-linkedin" href="#"><i class="fa fa-linkedin"></i></a></li>
							          <li><a class="faicon-facebook" href="#"><i class="fa fa-facebook"></i></a></li>
							          <li><a class="faicon-flickr" href="#"><i class="fa fa-flickr"></i></a></li>
							          <li><a class="faicon-rss" href="#"><i class="fa fa-rss"></i></a></li>
							        </ul>
								</div>
						</div>
						<div class="side animated slideInRight">
							<p class="sideTitle"><i class="fa fa-newspaper-o"></i>Subscribe To Our Newsletter:</p>
								<div class="sideBoxContent">
								<div class="sub">
								    <form action="index.html" method="post" class="subscribe-form">
								      <input type="email" name="email" class="subscribe-input" placeholder="Email address">
								      <button type="submit" class="subscribe-submit"><i class="fa fa-check-circle"></i></button>
								    </form>
								</div>
								</div>
						</div>

					</div>
					</div>