@extends('adminmaster')

@section('content')
<div class="col s12 m12">
						<div class="searchForm animated slideInDown " style="display:none">
							<form action="" method="" class="">
								<div class="input-field">		    
								<input id="searchIn1" placeholder="Search E-Books Here.." id="search" type="text" class="validate">	
									    	
								<input id="searchIn2" class="btn" type="submit" value="Search">	
								</div>
							</form>
						</div>
<div id="login-page" class="row">
    <div class="col s12 z-depth-4 card-panel">
      <form class="login-form" action="/updateUser" method="post">
        <div class="row">
          <div class="input-field col s12 center">
            <img src="img/office.jpg" alt="" class="circle responsive-img valign profile-image-login">
            <p class="center login-form-text">Update Admin</p>
          </div>
        </div>
        @if(!empty($users))

        <div class="row margin">
          <div class="input-field col s12">
            <i class="mdi-social-person-outline prefix"></i>
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <input id="fullname" type="text" name="fullname" value="{{ $users->fullname }}"/>
            <label for="fullname" class="center-align">Enter Your Full Name</label>
          </div>
        </div>
          <div class="row margin">
          <div class="input-field col s12">
            <i class="mdi-social-person-outline prefix"></i>
            <input type="hidden" name="id" value="{{ $users->id}}">
            <input id="email" type="text" name="email" value="{{ $users->email }}">
            <label for="email" class="center-align">Email Address</label>
          </div>
          </div>
         <div class="row margin">
          <div class="input-field col s12">
            <i class="mdi-social-person-outline prefix"></i>
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <input id="phone" type="text" name="phone" value="{{ $users->phone }}">
            <label for="phone" class="center-align">Phone No.</label>
          </div>
        </div>
        
        <div class="row margin">
          <div class="input-field col s12">
            <i class="mdi-action-lock-outline prefix"></i>
            <input id="username" type="text" name="username" value="{{ $users->username }}">
            <label for="username">Username</label>
          </div>
        </div>
        <div class="row margin">
          <div class="input-field col s12">
            <i class="mdi-action-lock-outline prefix"></i>
            
           
            <input id="password" type="text" name="password" value="" required>
            <label for="password">PassWord</label>
          </div>
        </div>

    
        <div class="row">
          <div class="input-field col s12">
            <button class="btn waves-effect waves-light col s12">update</button>
          </div>
        </div>


      </form>
      @endif
    </div>
  </div>
  <div class="main_content animated slideInDown">
								<ul class="ca-menu">
									<li>
										<a href="#">
											<span class="ca-icon">A</span>
											<div class="ca-content">
												<h2 class="ca-main">New Books</h2>
												<h3 class="ca-sub">Recent Uploaded Books</h3>
											</div>
										</a>
									</li>
									<li>
										<a href="#">
											<span class="ca-icon">B</span>
											<div class="ca-content">
												<h2 class="ca-main">Most Downloaded</h2>
												<h3 class="ca-sub">Most Downloaded Books</h3>
											</div>
										</a>
									</li>
									<li>
										<a href="#">
											<span class="ca-icon">C</span>
											<div class="ca-content">
												<h2 class="ca-main">Popular Books</h2>
												<h3 class="ca-sub">Popular E-Books</h3>
											</div>
										</a>
									</li>
								</ul>
						</div>

					</div>
@stop