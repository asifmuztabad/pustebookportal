				<div class="row">
					<div class="col s12 m12">
												<div class="catalog">
					 		<div class="catalogHeader">
					 			<p>E-Book Catalog</p>
					 		</div>
					 		<div class="catalogContent">
					 			<div class="row">
					 				<div class="col s4 m4 l4">
					 					<div class="catContent1">

										      <div class="row">
										        <div class="col s12 m12">
										          <div class="card">
										            <div class="card-image">
										              <img src="{{ URL::asset('img/office.jpg')}}">
										              <span class="card-title">Card Title</span>
										            </div>
										            <div class="card-content">
										              <p>I am a very simple card. I am good at containing small bits of information.
										              I am convenient because I require little markup to use effectively.</p>
										            </div>
										            <div class="card-action">
										              <a href="#">This is a link</a>
										            </div>
										          </div>
										        </div>
										      </div>	
					 					</div>
					 				</div>
					 				<div class="col s4 m4 l4">
					 					<div class="catContent2">

										      <div class="row">
										        <div class="col s12 m12">
										          <div class="card">
										            <div class="card-image">
										              <img src="{{ URL::asset('img/sample-1.jpg')}}">
										              <span class="card-title">Card Title</span>
										            </div>
										            <div class="card-content">
										              <p>I am a very simple card. I am good at containing small bits of information.
										              I am convenient because I require little markup to use effectively.</p>
										            </div>
										            <div class="card-action">
										              <a href="#">This is a link</a>
										            </div>
										          </div>
										        </div>
										      </div>
					 					</div>
					 				</div>
					 				<div class="col s4 m4 l4">
					 					<div class="catContent1">

										      <div class="row">
										        <div class="col s12 m12">
										          <div class="card">
										            <div class="card-image">
										              <img src="{{ URL::asset('img/sample-1.jpg')}}">
										              <span class="card-title">Card Title</span>
										            </div>
										            <div class="card-content">
										              <p>I am a very simple card. I am good at containing small bits of information.
										              I am convenient because I require little markup to use effectively.</p>
										            </div>
										            <div class="card-action">
										              <a href="#">This is a link</a>
										            </div>
										          </div>
										        </div>
										      </div>	
					 					</div>
					 				</div>
					 			</div>
					 		</div>
					 	</div>
					</div>
				</div>