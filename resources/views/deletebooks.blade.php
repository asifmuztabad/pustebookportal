@extends('adminmaster')

@section('content')

<div class="col s12 m12">
						<div class="searchForm animated slideInDown " style="display:block">
<a href="{{ URL::to('deletebooks','A') }}">A</a> |
<a href="{{ URL::to('deletebooks','B') }}">B</a> |
<a href="{{ URL::to('deletebooks','C') }}">C</a> |
<a href="{{ URL::to('deletebooks','D') }}">D</a> |
<a href="{{ URL::to('deletebooks','E') }}">E</a> |
<a href="{{ URL::to('deletebooks','F') }}">F</a> |
<a href="{{ URL::to('deletebooks','G') }}">G</a> |
<a href="{{ URL::to('deletebooks','H') }}">H</a> |
<a href="{{ URL::to('deletebooks','I') }}">I</a> |
<a href="{{ URL::to('deletebooks','J') }}">J</a> |
<a href="{{ URL::to('deletebooks','K') }}">K</a> |
<a href="{{ URL::to('deletebooks','L') }}">L</a> |
<a href="{{ URL::to('deletebooks','M') }}">M</a> |
<a href="{{ URL::to('deletebooks','N') }}">N</a> |
<a href="{{ URL::to('deletebooks','O') }}">O</a> |
<a href="{{ URL::to('deletebooks','P') }}">P</a> |
<a href="{{ URL::to('deletebooks','Q') }}">Q</a> |
<a href="{{ URL::to('deletebooks','R') }}">R</a> |
<a href="{{ URL::to('deletebooks','S') }}">S</a> |
<a href="{{ URL::to('deletebooks','T') }}">T</a> |
<a href="{{ URL::to('deletebooks','U') }}">U</a> |
<a href="{{ URL::to('deletebooks','V') }}">V</a> |
<a href="{{ URL::to('deletebooks','W') }}">W</a> |
<a href="{{ URL::to('deletebooks','X') }}">X</a> |
<a href="{{ URL::to('deletebooks','Y') }}">Y</a> |
<a href="{{ URL::to('deletebooks','Z') }}">Z</a> |


							<style>
td, th {
    padding: 15px 5px;
    display: table-cell;
    text-align: center;
    vertical-align: middle;
    border-radius: 2px;
}
							</style>
						</div>
						@include('adminMenu')
						@if($users!=='0')
						 <div class="container-fluid aboutH">
						        <h5 class="copper">BookList By Alphabetic Order</h5>
						        <h6></h6>
						        <hr>
						        <div class="row">
						          <div class="col s12 m12">
						          <form action="/deletebooksd" method="post" enctype="multipart/form-data">
								      <table class="bordered">
								        <thead>
								          <tr>
								          		<th data-field="id">Check</th>
								              <th data-field="id">Ebook Name</th>
								              <th data-field="name">Author</th>

								              
								          </tr>
<?php 
$i=0;
?>								        </thead>
@foreach($users as $property)

								        <tbody>

								          <tr>
								          <td><input type="checkbox" id="myCheckbox<?php echo $i;?>" class="filled-in" name="id[]" value="{{$property->id}}"/>
<label for="myCheckbox<?php echo $i;$i++;?>"></label>
								          </td>
								            <td>{{$property->filetitle}}</td>
								            <td>{{$property->fileauthor}}</td>
								            
								          </tr>
								           

@endforeach
								        </tbody>
								      </table>

            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <button class="btn waves-effect waves-light">Delete</button>

								      
								      
								      </form>
								      <hr>
								      <button class="btn" id="select_all">CheckAll</button>
								      <button class="btn" id="deselect_all">UnCheckAll</button>
						          </div>
						          </div>
{!! $users->render() !!}
  <span class="paginationCustomText"> Showing {{($users->currentpage()-1)*$users->perpage()+1}} to {{$users->currentpage()*$users->perpage()}}
    of  {{$users->total()}} E-Books
</span>
						   </div>
@else
						 <div class="container-fluid aboutH">
						        <h5 class="copper">About Us</h5>
						        <h6>Let Us introduce Ourself</h6>
						        <hr>
						        <div class="row">
						          <div class="col s12 m12">
<h1>Sorry no Books Found!!!!</h1>
						          </div>
						          </div>

						   </div>

						   @endif



						   						<div class="main_content animated slideInDown">
								<ul class="ca-menu">
									<li>
										<a href="#">
											<span class="ca-icon">A</span>
											<div class="ca-content">
												<h2 class="ca-main">New Books</h2>
												<h3 class="ca-sub">Recent Uploaded Books</h3>
											</div>
										</a>
									</li>
									<li>
										<a href="#">
											<span class="ca-icon">B</span>
											<div class="ca-content">
												<h2 class="ca-main">Most Downloaded</h2>
												<h3 class="ca-sub">Most Downloaded Books</h3>
											</div>
										</a>
									</li>
									<li>
										<a href="#">
											<span class="ca-icon">C</span>
											<div class="ca-content">
												<h2 class="ca-main">Popular Books</h2>
												<h3 class="ca-sub">Popular E-Books</h3>
											</div>
										</a>
									</li>
								</ul>
						</div>

					</div>
					@stop
