<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Maketable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bookstore', function (Blueprint $table) {
            $table->increments('id');
            $table->string('filetitle', 1000);
            $table->string('bookfilepath', 1000);
            $table->string('fileauthor', 1000);
            $table->string('mimetype', 1000);
            $table->string('downloadaddress', 1000);
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('bookstore');
    }
}
