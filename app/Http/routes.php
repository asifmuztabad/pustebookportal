<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|searchAlphabetically
*/
Route::get('searchAlphabetically/{letter}','bookController@searchAlphabetically');
Route::get('getentry/{filename}','bookController@get');
//Route::get('fileentry/get/{filename}', [
	//'as' => 'getentry', 'uses' => 'bookController@get']);
Route::get('searchByName','bookController@searchByName');

Route::get('logoutUser','bookController@logoutUser');
Route::get('/loginUser', array('uses' => 'bookController@loginUser'));
Route::post('/loginUser', array('uses' => 'bookController@loginUser'));

Route::get('/getDownload', array('uses' => 'bookController@getDownload'));
Route::get('/urlfilesave', array('uses' => 'bookController@store'));
Route::post('/urlfilesave', array('uses' => 'bookController@store'));

Route::post('/deletebooksd', ['middleware' => 'auth','uses' => 'bookController@deletebooksd']);
//Route::get('/deletebooksd', ['middleware' => 'auth','uses' => 'bookController@deletebooksd']);
Route::get('/deletebooks/{letter}', ['middleware' => 'auth','uses' => 'bookController@deletebooks']);
Route::get('/urlfilesaveUpdate', ['middleware' => 'auth','uses' => 'bookController@urlfilesaveUpdate']);
Route::post('/urlfilesaveUpdate', ['middleware' => 'auth','uses' => 'bookController@urlfilesaveUpdate']);
Route::get('/editbook/{id}', ['middleware' => 'auth','uses' => 'bookController@editbook']);
Route::get('showbooks/{letter}', ['middleware' => 'auth','uses' => 'bookController@showbooks']);
Route::get('/deleteUser/{id}', ['middleware' => 'auth','uses' => 'bookController@deleteUser']);
Route::post('/deleteUser', ['middleware' => 'auth','uses' => 'bookController@deleteUser']);
Route::get('/updateUser', ['middleware' => 'auth','uses' => 'bookController@updateUser']);
Route::post('/updateUser', ['middleware' => 'auth','uses' => 'bookController@updateUser']);
Route::get('/editeuser/{id}', ['middleware' => 'auth','uses' => 'bookController@editeuser']);
Route::get('/showadmin', ['middleware' => 'auth','uses' => 'bookController@showadmin']);
Route::get('/registerUser', ['middleware' => 'auth','uses' => 'bookController@registerUser']);
Route::post('/registerUser',['middleware' => 'auth','uses' => 'bookController@registerUser']);
Route::get('/adminPanel',['middleware' => 'auth','uses' =>'bookController@adminPanel']);
Route::get('/upload',['middleware' => 'auth','uses' =>'bookController@upload']);


Route::resource('books', 'bookController');
Route::get('/home', function () {
    return view('home');
});
Route::get('/', function () {
    return view('home');
});

