<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Storeebooks;
use App\User;
use Hash;
use Auth;
use Response;
use Input;
use Illuminate\Support\Facades\Redirect;


class bookController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function upload()
    {
         $R=Auth::user();
         //var_dump($R);
        return View('upload');
    }
    public function adminPanel()
    {
         $R=Auth::user();
         //var_dump($R);
        return View('adminPanel');
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($request->has('filetitle') and $request->hasFile('pdffile')) {
            # code...
                  $ext=$request->file('pdffile')->getClientOriginalExtension();
                  if ($ext!='pdf') {
                      return"File Type Error";
                    }
                    else{
                        $filename=$request->input('filetitle');
                      $filename1=$request->file('pdffile')->getClientOriginalName();
                      $filemimetype=$request->file('pdffile')->getClientMimeType();
                      //echo $filemimetype;
                      $upload=$request->file('pdffile')->move(base_path().'/public/uploadebook',$filename);
                      //$add= $request->file('pdffile')->getRealPath();
                      //echo $add;
                      if ($upload) {
                        //return"Uploaded";
                    $fileauthor = $request->input('fileauthor');
                    $downloadaddress = $request->input('downloadaddress');
                    
                    $query=Storeebooks::create(array(
                                        'filetitle1'=>$filename1,
                                        'filetitle'=>$filename,
                                        'bookfilepath'     =>$upload ,
                                        'fileauthor' => $fileauthor,
                                        'mimetype' => $filemimetype,
                                        'downloadaddress' => $downloadaddress
                                    ));
                    //var_dump($query);
                    if ($query) {
                        # code...
                        
                        return Redirect::to('upload')->withMessage('Uploaded');
                    }
                          # code...
                      }
                    }

         }
    }

        public function urlfilesaveUpdate(Request $request)
    {
        if ($request->has('filetitle') and $request->hasFile('pdffile')) {
            # code...
            $id=$request->input('id');
                  $ext=$request->file('pdffile')->getClientOriginalExtension();
                  if ($ext!='pdf') {
                      return"File Type Error";
                    }
                    else{
                      $filename=$request->input('filetitle');
                       $filename1=$request->file('pdffile')->getClientOriginalName();
                      $filemimetype=$request->file('pdffile')->getClientMimeType();
                      //echo $filemimetype;
                      $upload=$request->file('pdffile')->move(base_path().'/public/uploadebook',$filename1);
                      //$add= $request->file('pdffile')->getRealPath();
                      //echo $add;
                      if ($upload) {
                        //return"Uploaded";
                    $fileauthor = $request->input('fileauthor');
                    $downloadaddress = $request->input('downloadaddress');
                    
                    $query=Storeebooks::where('id',$id)
                                        ->update([
                                            'filetitle1'=>$filename1,
                                        'filetitle'=>$filename,
                                        'bookfilepath'     =>$upload ,
                                        'fileauthor' => $fileauthor,
                                        'mimetype' => $filemimetype,
                                        'downloadaddress' => $downloadaddress
                                    ]);
                    //var_dump($query);
                    if ($query) {
                        # code...
                        
                        return Redirect::to('showbooks/A')->withMessage('Updated');
                    }
                    else
                         return Redirect::to('showbooks/A')->withMessage('Not Updated');
                      }
                    }

         }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }
    public function showadmin()
    {
      $users = User::all();
      if(!empty($users)){
       return View('showadmin',compact('users')); 
      }
      else{
        return Redirect::to('showadmin')->withMessage('No User Found');
      }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function editeuser($id)
    {
        if(!empty($id)){
            $users = User::where('id',$id)->first();
            if ($users) {
                //var_dump($users);
                return View('editeuser',compact('users')); 
            }
            else{
               return Redirect::to('showadmin')->withMessage('Sorry No User Found');  
            }
        }
        else{
           return Redirect::to('showadmin')->withMessage('No User ID Found'); 
        }
    }
    public function editbook($id)
    {
        if(!empty($id)){
            $users = Storeebooks::where('id',$id)->first();
            if ($users) {
                //var_dump($users);
                return View('editbook',compact('users')); 
            }
            else{
               return Redirect::to('showbooks/A')->withMessage('Sorry No Books Found');  
            }
        }
        else{
           return Redirect::to('showbooks/A')->withMessage('No Book ID Found'); 
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function registerUser(Request $request)
    {
         $fullname = $request->input('fullname');
         $username = $request->input('username');
         $password = $request->input('password');
         $email = $request->input('email');
         $phone = $request->input('phone');

         if ($request->has('fullname') and $request->has('username') and $request->has('password') and $request->has('email')) {
             # code...
                                $query=User::create(array(
                                        'username'=>$username,
                                        'password'=>Hash::make($password),
                                        'fullname'=> $fullname,
                                        'email' => $email,
                                        'phone' => $phone
                                    ));
                        if ($query) {
                        # code...
                        
                        return Redirect::to('showadmin')->withMessage('Registered');
                    }
            else{
            return Redirect::to('showadmin')->withMessage('Does not Registere');
         }
         }
         else{
            return View('register')->withMessage('Please Insert Registere');
         }
    }
    public function loginUser(Request $request)
    {
     
         $username = $request->input('username');
         $password = $request->input('password');
        
         

         if ($request->has('username') and $request->has('password')) {
             # code...
                                $query=array(
                                        'username'=>$username,
                                        'password'=>$password
                                        
                               
                                    );
                                    if (Auth::attempt($query)) {
  return Redirect::to('adminPanel')->withMessage('Logged In Successfully');
    } 
    else { 
      



return Redirect::to('loginUser')->withMessage('Wrong Username And Password');
    }

    }
    else
    {
       return View('login'); 
    }
    }
public function logoutUser()
    {
        Auth::logout(); // log the user out of our application
    
    return Redirect::to('/'); // redirect the user to the login screen
    } 
    public function searchByName(Request $request)
    {
      $bookname = $request->input('bookname');
      if (!empty($bookname)) {
          # code...
     
     $users = Storeebooks::where('filetitle', 'LIKE', '%'.$bookname.'%')->paginate(3,["filetitle","fileauthor","bookfilepath"],"p");
     //$total=$results->total();
     $users->appends(['bookname' => $bookname]);

     //var_dump($users) ;
    return View('searchByName',compact('users')); 
      } 
      else{
$users="";
    return View('searchByName',compact('users')); // redirect the user to the login screen
}
    }

    public function get($filename){

        $entry = Storeebooks::where('filetitle', '=', $filename)->firstOrFail();
        //var_dump( $entry);
        $filepath=$entry->bookfilepath;
        $filename1=$entry->filetitle1;
    $headers = array('Content-Type: application/pdf');
 $response=Response::download($filepath, $filename1, $headers);
 ob_end_clean();
 return $response;
        //return (new Response($file, 200))
             // ->header('Content-Type', $entry->mimetype);
       
    } 
public function searchAlphabetically($letter)
    {
      
      if (!empty($letter)) {
          # code...
     
  $users = Storeebooks::where('filetitle', 'LIKE',$letter.'%')->paginate(3,["filetitle","fileauthor","bookfilepath"],"p");
     //$total=$results->total();
     $users->appends(['letter' => $letter]);

     //var_dump($users) ;
    return View('searchAlphabetically',compact('users')); 
      } 
      else{
$users='0';
    return View('searchAlphabetically',compact('users')); // redirect the user to the login screen
}
    }


    public function updateUser(Request $request)
    {
         $fullname = $request->input('fullname');
         $username = $request->input('username');
         $password = $request->input('password');
         $email = $request->input('email');
         $phone = $request->input('phone');
         $id = $request->input('id');
         if ($request->has('fullname') and $request->has('username') and $request->has('password') and $request->has('email')) {
             # code...
                  $query=User::where('id', $id)
                              
                                ->update([
                                        'username'=>$username,
                                        'password'=>Hash::make($password),
                                        'fullname'=> $fullname,
                                        'email' => $email,
                                        'phone' => $phone
                                    ]);
                        if ($query) {
                        # code...
                        
                        return Redirect::to('showadmin')->withMessage('Updated');
                    }
         }
         else{
            return Redirect::to('showadmin')->withMessage('Does not Update');
         }
    }

        public function deleteUser($id)
    {

     
             # code...
                  $query=User::where('id', $id)
                              
                                ->delete();
                        if ($query) {
                        # code...
                        
                        return Redirect::to('showadmin')->withMessage('Deleted');
                    }
        
         else{
            return Redirect::to('showadmin')->withMessage('Does not Deleted');
         }
    }
            public function deletebooksd(Request $request)
    {

     
          $id = Input::get('id');
          var_dump($id);
          foreach($id as $i ) {


                  $query=Storeebooks::where('id', $i)
                              
                                ->delete();
                            }
                        if ($query) {
                        # code...
                        
                        return Redirect::to('deletebooks/A')->withMessage('Deleted');
                    }
        
         else{
            return Redirect::to('deletebooks/A')->withMessage('Does not Deleted');
         }
    }

    public function deletebooks($letter)
    {

           if (!empty($letter)) {
          # code...
     
  $users = Storeebooks::where('filetitle', 'LIKE',$letter.'%')->paginate(10,["id","filetitle","fileauthor","bookfilepath"],"p");
     //$total=$results->total();
     $users->appends(['letter' => $letter]);

     //var_dump($users) ;
    return View('deletebooks',compact('users')); 
      } 
      else{
$users='0';
    return View('deletebooks',compact('users')); // redirect the user to the login screen
}
        
    }
    public function showbooks($letter)
    {
      
      if (!empty($letter)) {
          # code...
     
  $users = Storeebooks::where('filetitle', 'LIKE',$letter.'%')->paginate(10,["id","filetitle","fileauthor","bookfilepath"],"p");
     //$total=$results->total();
     $users->appends(['letter' => $letter]);

     //var_dump($users) ;
    return View('showbooks',compact('users')); 
      } 
      else{
$users='0';
    return View('showbooks',compact('users')); // redirect the user to the login screen
}
    }
}
