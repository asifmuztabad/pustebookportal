<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Storeebooks extends Model
{
    	protected $table = 'bookstore';
		protected $fillable =['filetitle','filetitle1', 'bookfilepath','fileauthor','mimetype','downloadaddress'];
}
