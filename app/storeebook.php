<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class storeebook extends Model
{
    	protected $table = 'bookstore';
		protected $fillable =['filetitle', 'bookfilepath','fileauthor','filepath','downloadaddress'];
}
